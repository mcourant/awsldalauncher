import json
import sys
import os

with open('config/config.json') as json_data_file:
    data = json.load(json_data_file)

with open('config/event.json') as json_data_file:
    event = json.load(json_data_file)

#Setup db settings
if sys.argv[1] == "prod":
    dict_db = data["environment_prod"]
else:
    dict_db = data["environment_dev"]
for count, (key, value) in enumerate(dict_db.items(), 1):
    os.environ[key] = value

try:
    file_source = __import__("files"+"."+sys.argv[1]+"."+sys.argv[2], fromlist=["files."+sys.argv[1]])
    print(file_source.handler(event, data["general"]["context"]))
except ImportError:
    print("We don't find your file ! See the same name one your command / file !")
except TypeError:
    print("Check your parameters in config.json !")    

