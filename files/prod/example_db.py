import sys
import logging
import json
import pymysql
import os
#rds settings
host  = os.environ["db_host"]
user = os.environ["db_user"]
password = os.environ["db_password"]
db_name = os.environ["db_database"]

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def handler(event, context):
    try:
        conn = pymysql.connect(host=host, user=user, passwd=password, db=db_name, connect_timeout=5)
    except:
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        sys.exit()
    
    logger.info("SUCCESS: Connection to mysql instance succeeded")
    with conn.cursor(pymysql.cursors.DictCursor) as cur:
        #Code here
        return "Do what you want with the cursor"
        

