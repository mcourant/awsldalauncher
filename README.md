## AWS Lambda launch python localy
A function where you can run your lambda function on your computer in python

## What you need ?
Check if you have python3 with this :

    python3 --version
If nothing is displayed install python3 on your computer
Have a command line can launch python3

## Configure your database evironment
Go to config.json file, you will found two environment variable:

    "environment_dev" : {
	    "db_host" : "urltodev.sql",
	    "db_user" : "username",
	    "db_password" : "password",
	    "db_database" : "table"
	},
	"environment_prod" : {
		"db_host" : "urltoprod.sql",
		"db_user" : "username",
		"db_password" : "password",
		"db_database" : "teester"
	}

## Add parameters variable
Go to event.json and paste your parameters for event variable in your handler

## How launch that ?
Run that command : 

    sudo python3 lambda.py <env> <file>

Remplace "env" with your environment prod/dev.
Remplace "file" with the name of your file you want to test
